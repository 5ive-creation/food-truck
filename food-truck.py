#!/usr/bin/python3
import argparse
import requests
import csv
from io import StringIO
import random
import re


class Application:
    def get_food_truck_data(self, args):
    # TODO: Optionally, we could cache this data and use a TTL to limit hitting this API.
    #       This data most likely doesn't update terribly frequently
        response = requests.get(args.api_url)

        if not response or not response.status_code == 200:
            print("ERROR: unable to retrieve food truck data")
            exit(1)

        food_truck_data = csv.DictReader(StringIO(response.text))
        return list(food_truck_data)

    def valid_choice(self, args, food_truck):
        """Returns boolean depending on if this food truck's registration is valid and reflects the user's criteria."""
        if not food_truck:
            return False

        if not food_truck["Status"].upper() == "APPROVED":
            return False

        # TODO: Improve by joining the user's include and exclude arguments as a single regex.
        #       Then compile that regex once and reuse as an input for performance.
        for include in args.include:
            if not re.search(include, food_truck["FoodItems"], re.IGNORECASE):
                return False

        for exclude in args.exclude:
            if re.search(exclude, food_truck["FoodItems"], re.IGNORECASE):
                return False

        return True

    def get_random_food_truck(self, food_truck_data):
        """Returns a random food truck by popping from the list. Which ensures this can be done multiple times with unique results."""
        random_truck = random.randint(0, len(food_truck_data) - 1)
        return food_truck_data.pop(random_truck)

    def print_random_food_truck(self, args, food_truck_data, limit):
    # Not thread-safe, but using a recursive function safely allows removing elements from the list.
    # The recursive function is also a cleaner way of managing the count and limit argument.
        if limit < 1:
            return

        for i in range(1, len(food_truck_data)):
            food_truck = self.get_random_food_truck(food_truck_data)
            if self.valid_choice(args, food_truck):
                print(
                    """
{name}
{address}
{food}
                """.format(
                        name=food_truck["Applicant"],
                        address=food_truck["Address"],
                        food=food_truck["FoodItems"],
                    )
                )
                break

        self.print_random_food_truck(args, food_truck_data, limit - 1)

    def __init__(self):
        parser = argparse.ArgumentParser(
            description="Help foodies choose their next food truck. Prints out a listing taken at random from SF API data.",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        )
        parser.add_argument(
            "--api-url",
            help="URL to the public CSV data",
            default="https://data.sfgov.org/api/views/rqzj-sfat/rows.csv",
        )
        parser.add_argument(
            "--exclude",
            help="Exclude trucks containing any of these food keywords",
            nargs="*",
            default=[],
        )
        parser.add_argument(
            "--include",
            help="Include trucks containing all of these food keywords",
            nargs="*",
            default=[],
        )
        parser.add_argument(
            "--limit",
            help="Limit the results returned by this value",
            type=int,
            default=10,
        )
        args = parser.parse_args()

        food_truck_data = self.get_food_truck_data(args)
        limit = args.limit
        # Avoid recursion depth error if the value is set extremely high
        if args.limit > len(food_truck_data):
            limit = len(food_truck_data)

        self.print_random_food_truck(args, food_truck_data, limit)


if __name__ == "__main__":
    app = Application()

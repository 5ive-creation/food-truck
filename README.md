# Food Truck Randomizer

This application helps foodies select from a random list of food trucks, which match their criteria.

## Executing in a virtual environment

git clone https://gitlab.com/5ive-creation/food-truck.git

cd food-truck

python -m venv .venv

source .venv/bin/activate

pip install -r requirements.txt

$ `python ./food-truck.py --help`

usage: food-truck.py [-h] [--api-url API_URL] [--exclude [EXCLUDE ...]] [--include [INCLUDE ...]] [--limit LIMIT]

Help foodies choose their next food truck

options:

  -h, --help            show this help message and exit

  --api-url API_URL     URL to the public CSV data (default: https://data.sfgov.org/api/views/rqzj-sfat/rows.csv)

  --exclude [EXCLUDE ...]
                        Exclude trucks containing any of these food keywords (default: [])

  --include [INCLUDE ...]
                        Include trucks containing all of these food keywords (default: [])

  --limit LIMIT         Limit the results returned by this value (default: 10)

## Usage Examples

Here are multiple examples, which will generate random results each time, but gives all scenarios in which the command can be executed.

1. Search only for trucks offering soda. Limited to 3 results
   
   $ `python ./food-truck.py --include soda --limit 3`
   
   Truly Food & More
   602 MISSION ST
   Latin Food: Tacos: Pupusas: Vegetables: Salad: Waters: Sodas
   
   San Pancho's Tacos
   3119 ALEMANY BLVD
   Tacos: Burritos: Quesadillas: Tortas: Nachos: Hot Dogs:Soda: Water: Fruit Drinks
   
   Truly Food & More
   217 SANSOME ST
   Latin Food: Tacos: Pupusas: Vegetables: Salad: Waters: Sodas

2. Exclude Tortas from the results. Case is insensitive
   
   $ `python ./food-truck.py --include soda --limit 3 --exclude torta`
   
   The New York Frankfurter Co. of CA, Inc. DBA: Annie's Hot Dogs
   101 STOCKTON ST
   Soft Pretzels: hot dogs: sausage: chips: popcorn: soda: espresso: cappucino: pastries: ice cream: italian sausages: shish-ka-bob: churros: juice: water: various drinks
   
   The New York Frankfurter Co. of CA, Inc. DBA: Annie's Hot Dogs
   870 MARKET ST
   Soft pretzels: hot dogs: sausages: chips: popcorn: soda: espresso: cappucino: pastry: ica cream: ices: italian sausage: shish-ka-bob: churros: juice: water: various drinks
   
   Truly Food & More
   602 MISSION ST
   Latin Food: Tacos: Pupusas: Vegetables: Salad: Waters: Sodas

3. Multiple exclusion and inclusion terms are allowed
   
   $ `python ./food-truck.py --include soda --limit 3 --exclude torta espresso`
   
   El Calamar Perubian Food Truck
   85 02ND ST
   Lomo Saltado: Jalea: Ceviche: Calamar: Tilapia plate: chicken special. Soda: Water.
   
   Truly Food & More
   505 HOWARD ST
   Latin Food: Tacos: Pupusas: Vegetables: Salad: Waters: Sodas
   
   Truly Food & More
   602 MISSION ST
   Latin Food: Tacos: Pupusas: Vegetables: Salad: Waters: Sodas
